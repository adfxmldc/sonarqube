@echo off
setlocal
set THIS_DIR=%~dp0
set BASE_DIR=%THIS_DIR%\..\
set DB_NAME=sonar
set SQL_DUMP=%THIS_DIR%\sonar.sql
set Path=%Path%;C:\Oracle\Java\jdk1.7.0_25\bin

set MYSQL_DIR=%BASE_DIR%\mysql\
set SONAR_DIR=%BASE_DIR%\sonarqube\

echo Stopping mysql in case it is still running...
%MYSQL_DIR%\bin\mysqladmin -u root shutdown
echo Wait for shutdown to fully complete...
ping 192.0.2.2 -n 3 -w 1000 > nul

echo Delete MySQL data files...
rmdir /S /Q %MYSQL_DIR%\data\

echo Unzip seeder database...
unzip -q %THIS_DIR%\init-mysql-db.zip -d %MYSQL_DIR%\data\

echo Start MySql...
start %MYSQL_DIR%\bin\mysqld

echo Create mysql sonar user...
%MYSQL_DIR%\bin\mysql -u root  --execute="create user 'sonar' identified by 'sonar'; grant all on sonar.* TO 'sonar'@'localhost' identified by 'sonar'; flush privileges;"

echo Import database dump...
%MYSQL_DIR%\bin\mysql -u root < %SQL_DUMP%

echo Deleting potential stale SonarQube search cache...
rmdir /S /Q %SONAR_DIR%\data\es\

echo Deleting SonarQube logs and temp files...
rmdir /S /Q %SONAR_DIR%\logs\
rmdir /S /Q %SONAR_DIR%\temp\

echo Start SonarQube...
echo ############################################################################################################
echo ### to shutdown SonarQube:                                                                               ###
echo ### press ctrl-C, wait for SonarQube to fully shut down and say N to question asking to abort batch file ###
echo ### that will continue this batch file and export (and shutdown) the MySql database                      ###
echo ############################################################################################################
call %SONAR_DIR%\bin\windows-x86-64\StartSonar.bat

echo Create database dump...
rem dump file as friendly for version control as possible (no data in comment, one insert per line and insert ordering deterministic)
%MYSQL_DIR%\bin\mysqldump -u root --skip-dump-date --skip-extended-insert --order-by-primary --databases %DB_NAME% > %SQL_DUMP%

echo Stopping mysql...
%MYSQL_DIR%\bin\mysqladmin -u root shutdown


endlocal